package com.rocketcase.clioclient.domain;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;

@Entity
@Table(name = "packages")
public class Package implements BaseEntity, java.io.Serializable {

	@Transient
	public static final String SINGLE = "package";
	@Transient
	public static final String URL_PRE_FIX = "package";

	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";
	
	@Transient
	public static final String GET_BY_ALIAS = URL_PRE_FIX + "/alias/";

	@Transient
	public static final String GET_BY_APPLICATION_URL = URL_PRE_FIX + "/application/";

	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";

	@Transient
	public static final String SAVE_URL = URL_PRE_FIX + "/save";

	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";

	private static final long serialVersionUID = 1L;
	private Long id;
	private String cbId;
	private String name;
	private String description;
	private Long maxCaseCount;
	private Double price;
	private String status;
	

	public Package() {
	}

	public Package(Long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "max_case_count")
	public Long getMaxCaseCount() {
		return maxCaseCount;
	}

	public void setMaxCaseCount(Long maxCaseCount) {
		this.maxCaseCount = maxCaseCount;
	}

	@Column(name = "price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	@Column(name = "cb_id")
	public String getCbId() {
		return cbId;
	}

	public void setCbId(String cbId) {
		this.cbId = cbId;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	

}
