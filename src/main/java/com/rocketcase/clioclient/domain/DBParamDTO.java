package com.rocketcase.clioclient.domain;

public class DBParamDTO {

	public String jdbc_driver_classname;
	public String jdbc_url;
	public String jdbc_user_name;
	public String jdbc_password;
	public String c3p0_initial_poolsize;
	public String c3p0_min_poolsize;
	public String c3p0_max_poolsize;
	public String c3p0_acquire_increment;
	public String c3p0_max_idle_time;
	public String c3p0_max_idle_time_excess_connections;
	public String c3p0_unreturned_connection_timeout;

	public String hibernate_dialect;
	public String hibernate_show_sql;
	public String hibernate_format_sql;

	public String getJdbc_driver_classname() {
		return jdbc_driver_classname;
	}

	public void setJdbc_driver_classname(String jdbc_driver_classname) {
		this.jdbc_driver_classname = jdbc_driver_classname;
	}

	public String getJdbc_url() {
		return jdbc_url;
	}

	public void setJdbc_url(String jdbc_url) {
		this.jdbc_url = jdbc_url;
	}

	public String getJdbc_user_name() {
		return jdbc_user_name;
	}

	public void setJdbc_user_name(String jdbc_user_name) {
		this.jdbc_user_name = jdbc_user_name;
	}

	public String getJdbc_password() {
		return jdbc_password;
	}

	public void setJdbc_password(String jdbc_password) {
		this.jdbc_password = jdbc_password;
	}

	public String getC3p0_initial_poolsize() {
		return c3p0_initial_poolsize;
	}

	public void setC3p0_initial_poolsize(String c3p0_initial_poolsize) {
		this.c3p0_initial_poolsize = c3p0_initial_poolsize;
	}

	public String getC3p0_min_poolsize() {
		return c3p0_min_poolsize;
	}

	public void setC3p0_min_poolsize(String c3p0_min_poolsize) {
		this.c3p0_min_poolsize = c3p0_min_poolsize;
	}

	public String getC3p0_max_poolsize() {
		return c3p0_max_poolsize;
	}

	public void setC3p0_max_poolsize(String c3p0_max_poolsize) {
		this.c3p0_max_poolsize = c3p0_max_poolsize;
	}

	public String getC3p0_acquire_increment() {
		return c3p0_acquire_increment;
	}

	public void setC3p0_acquire_increment(String c3p0_acquire_increment) {
		this.c3p0_acquire_increment = c3p0_acquire_increment;
	}

	public String getC3p0_max_idle_time() {
		return c3p0_max_idle_time;
	}

	public void setC3p0_max_idle_time(String c3p0_max_idle_time) {
		this.c3p0_max_idle_time = c3p0_max_idle_time;
	}

	public String getC3p0_max_idle_time_excess_connections() {
		return c3p0_max_idle_time_excess_connections;
	}

	public void setC3p0_max_idle_time_excess_connections(String c3p0_max_idle_time_excess_connections) {
		this.c3p0_max_idle_time_excess_connections = c3p0_max_idle_time_excess_connections;
	}

	public String getC3p0_unreturned_connection_timeout() {
		return c3p0_unreturned_connection_timeout;
	}

	public void setC3p0_unreturned_connection_timeout(String c3p0_unreturned_connection_timeout) {
		this.c3p0_unreturned_connection_timeout = c3p0_unreturned_connection_timeout;
	}

	public String getHibernate_dialect() {
		return hibernate_dialect;
	}

	public void setHibernate_dialect(String hibernate_dialect) {
		this.hibernate_dialect = hibernate_dialect;
	}

	public String getHibernate_show_sql() {
		return hibernate_show_sql;
	}

	public void setHibernate_show_sql(String hibernate_show_sql) {
		this.hibernate_show_sql = hibernate_show_sql;
	}

	public String getHibernate_format_sql() {
		return hibernate_format_sql;
	}

	public void setHibernate_format_sql(String hibernate_format_sql) {
		this.hibernate_format_sql = hibernate_format_sql;
	}

}
