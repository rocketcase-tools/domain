package com.rocketcase.clioclient.domain.clio.apiv4;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BillMatterPK implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long matterId;
	private Long billId;

	public BillMatterPK() {
	}

	
	
	public BillMatterPK(Long billId, Long matterId ) {
		super();
		this.billId = billId;
		this.matterId = matterId;
	}



	@Column(name = "matter_id")
	public Long getMatterId() {
		return this.matterId;
	}

	public void setMatterId(Long matterId) {
		this.matterId = matterId;
	}

	@Column(name = "bill_id")
	public Long getBillId() {
		return this.billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof BillMatterPK)) {
			return false;
		}
		BillMatterPK castOther = (BillMatterPK) other;
		return this.matterId.equals(castOther.matterId) && this.billId.equals(castOther.billId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.matterId.hashCode();
		hash = hash * prime + this.billId.hashCode();

		return hash;
	}
}
