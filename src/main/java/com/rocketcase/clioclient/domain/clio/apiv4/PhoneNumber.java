package com.rocketcase.clioclient.domain.clio.apiv4;

import java.lang.reflect.Field;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_phone_numbers")
public class PhoneNumber implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9087953359325064538L;

	@Expose @SerializedName("id")
	private Long id;

	@Expose @SerializedName("name")
	private String name;

	@Expose @SerializedName("number")
	private String number;

	@Expose @SerializedName("default_number")
	private String defaultNumber;

	@Expose @SerializedName("created_at")
	private Date createdAt;

	@Expose @SerializedName("updated_at")
	private Date updatedAt;

	private Contact contact;

	private Long contactId;

	public PhoneNumber() {
	}

	public PhoneNumber(Long id) {
		this.id = id;
	}

	public PhoneNumber(Long id, Contact contact, String name, String number, String defaultNumber, Date createdAt,
			Date updatedAt) {
		this.id = id;
		this.contact = contact;
		this.name = name;
		this.number = number;
		this.defaultNumber = defaultNumber;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "clio_contact_id")
	 */
	@Transient
	public Contact getContact() {
		return this.contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Column(name = "name" )
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "number" )
	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Column(name = "default_number")
	public String getDefaultNumber() {
		return this.defaultNumber;
	}

	public void setDefaultNumber(String defaultNumber) {
		this.defaultNumber = defaultNumber;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at" )
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at" )
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	
	
	@Column(name = "clio_contact_id")
	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}
	
	
	public static String getExposedFields(Boolean includeChildrens ) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = PhoneNumber.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
			}
		}

		return exposedFields.toString();

	} 
}
