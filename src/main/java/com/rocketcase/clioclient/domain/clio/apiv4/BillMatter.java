package com.rocketcase.clioclient.domain.clio.apiv4;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "clio_bills_matters")
public class BillMatter implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BillMatterPK id;

	public BillMatter() {
	}

	public BillMatter(BillMatterPK id) {
		super();
		this.id = id;
	}

	@EmbeddedId
	public BillMatterPK getId() {
		return id;
	}

	public void setId(BillMatterPK id) {
		this.id = id;
	}

}
