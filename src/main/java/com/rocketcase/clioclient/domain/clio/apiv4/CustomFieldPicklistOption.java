package com.rocketcase.clioclient.domain.clio.apiv4;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomFieldPicklistOption implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("option")
	private String option;

	
	
	
	
	
	
	
	public CustomFieldPicklistOption() {
		
	}
	public CustomFieldPicklistOption(Long id, String option) {
		super();
		this.id = id;
		this.option = option;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getOption() {
		return option;
	}
	public void setOption(String option) {
		this.option = option;
	}

	 

}
