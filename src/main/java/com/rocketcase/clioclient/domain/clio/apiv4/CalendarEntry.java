package com.rocketcase.clioclient.domain.clio.apiv4;

import java.lang.reflect.Field;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_calendar_entries")
public class CalendarEntry implements java.io.Serializable {
	 
	
 
	private static final long serialVersionUID = 4356585868639744221L;

	public static final String API_URL = "calendar_entries";
	public static final String MULTIPLE_KEY = "calendar_entries";
	public static final String SINGLE_KEY = "calendar_entry";


	
	@Expose
	@SerializedName("id")
	private String id;
	
	@Expose
	@SerializedName("created_at")
	private String createdAt;
	
	@Expose
	@SerializedName("updated_at")
	private String updatedAt;
	
	@Expose
	@SerializedName("start_at")
	private String startAt;
	
	@Expose
	@SerializedName("end_at")
	private String endAt;
	
	 
	@Expose
	@SerializedName("summary")
	private String summary;
	
	@Expose
	@SerializedName("description")
	private String description;
	
	@Expose
	@SerializedName("location")
	private String location;
	
	@Expose
	@SerializedName("matter")
	private Matter matter;

	private Long customerId;
	
	  

	
	
	
	/*private String startDateStr;
	private String endDateStr;
	private String startDateTimeStr;
	private String endDateTimeStr;
	private String matterUniId;*/
	
	
	
	public CalendarEntry() {
	}

	public CalendarEntry(String id) {
		this.id = id;
	}
	public CalendarEntry(String id, String startAt , String endAt , String summary, String matterUniId, Long matterId) {
		this.id = id;
		this.startAt = startAt;
		this.endAt= endAt;
		this.summary = summary;
		this.matter = new Matter();
		this.matter.setCustomFieldUniqueID(matterUniId);
		this.matter.setId(matterId);
	}
	public CalendarEntry(String id, String startAt , String endAt , String summary, Long matterId) {
		this.id = id;
		this.startAt = startAt;
		this.endAt= endAt;
		this.summary = summary;
		this.matter = new Matter();
		this.matter.setId(matterId);
	}
	/*public CalendarEntry(Long id, String startDateTimeStr, String endDateTimeStr, String summary, String matterUniqueID) {
		this.id = id;
		this.startDateTimeStr = startDateTimeStr;
		this.endDateTimeStr = endDateTimeStr;
		this.summary = summary;
		this.matter = new Matter();
		this.matter.setCustomFieldUniqueID(matterUniqueID);
	}
*/
	public CalendarEntry(String id, String createdAt, String updatedAt, String startAt , String endAt, String summary, String description, String location, Matter matter) {
		this.id = id;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.startAt = startAt;
		this.endAt= endAt;
		 
		this.summary = summary;
		this.description = description;
		this.location = location;
		this.matter  = matter ;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "created_at")
	public String getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updated_at")
	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_at")
	public String getStartAt() {
		return this.startAt;
	}

	public void setStartAt(String startAt) {
		this.startAt = startAt;
	}

	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_at")
	public String getEndAt() {
		return this.endAt;
	}

	public void setEndAt(String endAt) {
		this.endAt = endAt;
	}

	 

	@Column(name = "summary")
	public String getSummary() {
		return this.summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "location")
	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "matter_id")
	public Matter getMatter() {
		return matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	@Column(name = "customer_id")
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = CalendarEntry.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				/*if("matter".equals(annotation.value())){
					continue;
				}*/
				
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
				if (includeChildrens) {
					if ("matter".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Matter.getExposedFields(false));
						exposedFields.append("}");
					}
				}
			}
		}

		return exposedFields.toString();

	}
	
	
	 
	
	
	
	
	
	
	
	
	
	
	
	
}
