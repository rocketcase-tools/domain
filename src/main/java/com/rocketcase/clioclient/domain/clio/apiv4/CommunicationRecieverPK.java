package com.rocketcase.clioclient.domain.clio.apiv4;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CommunicationRecieverPK implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long communicationId;
	private Long contactId;

	public CommunicationRecieverPK() {
	}
	
	

	public CommunicationRecieverPK(Long communicationId, Long contactId) {
		super();
		this.communicationId = communicationId;
		this.contactId = contactId;
	}



	@Column(name="communication_id")
	public Long getCommunicationId() {
		return this.communicationId;
	}
	public void setCommunicationId(Long communicationId) {
		this.communicationId = communicationId;
	}

	@Column(name="contact_id")
	public Long getContactId() {
		return this.contactId;
	}
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CommunicationRecieverPK)) {
			return false;
		}
		CommunicationRecieverPK castOther = (CommunicationRecieverPK)other;
		return 
			this.communicationId.equals(castOther.communicationId)
			&& this.contactId.equals(castOther.contactId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.communicationId.hashCode();
		hash = hash * prime + this.contactId.hashCode();
		
		return hash;
	}
}
