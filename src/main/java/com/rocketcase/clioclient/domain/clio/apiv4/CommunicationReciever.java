package com.rocketcase.clioclient.domain.clio.apiv4;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_communications_recievers")
public class CommunicationReciever implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private CommunicationRecieverPK pkId;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("type")
	private String type;

	@Expose
	@SerializedName("id")
	private Long participantId;

	public CommunicationReciever() {
	}

	public CommunicationReciever(CommunicationRecieverPK pkId) {
		super();
		this.pkId = pkId;
	}

	public CommunicationReciever(Long communicationId, Long contactId) {
		super();
		this.pkId = new CommunicationRecieverPK(communicationId, contactId);
	}

	@EmbeddedId
	public CommunicationRecieverPK getPkId() {
		return pkId;
	}

	public void setPkId(CommunicationRecieverPK pkId) {
		this.pkId = pkId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Transient
	public Long getParticipantId() {
		return participantId;
	}

	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}
}
