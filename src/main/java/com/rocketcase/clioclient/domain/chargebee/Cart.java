package com.rocketcase.clioclient.domain.chargebee;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.User;
import com.rocketcase.clioclient.domain.base.BaseEntity;

@Entity
@Table(name = "carts")
public class Cart implements BaseEntity, java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3145088357194564403L;
	/**
	 * 
	 */
	 
	@Transient
	public static final String URL_PRE_FIX = "cart";
	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";
	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";
	@Transient
	public static final String GET_BY_USER_URL = URL_PRE_FIX + "/user/";
	
	@Transient
	public static final String SAVE_URL = URL_PRE_FIX + "/save";
	
	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";	

	/**
	 * 
	 */
	private Long id;
	private String cartNo;
	private String caseIds;
	private User user;

	private Set<Long> selectedCases;
	
	
	
	
	public Cart() {
	}
	
	

	public Cart( String cartNo, String caseIds, User user) {
		super();
		 
		this.cartNo = cartNo;
		this.caseIds = caseIds;
		this.user = user;
	}



	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	 

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	@Column(name="cart_no")
	public String getCartNo() {
		return cartNo;
	}

	public void setCartNo(String cartNo) {
		this.cartNo = cartNo;
	}

	@Column(name="case_ids")
	public String getCaseIds() {
		return caseIds;
	}

	public void setCaseIds(String caseIds) {
		this.caseIds = caseIds;
	}



	@Transient
	public Set<Long> getSelectedCases() {
		 
		return selectedCases;
	}



	public void setSelectedCases(Set<Long> selectedCases) {
		this.selectedCases = selectedCases;
	}
	
	
	
	
	 
	
	

	 
}
