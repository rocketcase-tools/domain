package com.rocketcase.clioclient.domain.chargebee;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;

@Entity
@Table(name = "carts_log")
public class CartLog implements BaseEntity, java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3145088357194564403L;
	/**
	 * 
	 */

	@Transient
	public static final String URL_PRE_FIX = "cartlog";
	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";
	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";

	@Transient
	public static final String GET_BY_USER_URL = URL_PRE_FIX + "/user/";

	@Transient
	public static final String GET_BY_CART_NO = URL_PRE_FIX + "/cart_no/";

	@Transient
	public static final String GET_BY_CART_ID = URL_PRE_FIX + "/cart_id/";

	@Transient
	public static final String GET_BY_INVOICE_NO= URL_PRE_FIX + "/invoice_no/";

	@Transient
	public static final String GET_BY_PURCHASE_DATE = URL_PRE_FIX + "/purchase_date/";

	@Transient
	public static final String GET_BY_CUSTOMER_ID= URL_PRE_FIX + "/customer/";

	@Transient
	public static final String SAVE_URL = URL_PRE_FIX + "/save";

	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";

	/**
	 * 
	 */
	private Long id;
	private Long cartId;
	private String cartNo;
	private String invoiceNo;
	private Date purchaseDate;
	private Long customerId;
	private String caseIds;
	private Long userId;
	
	private Set<Long> selectedCases;

	public CartLog() {
	}

	public CartLog(Long cartId, String cartNo, String invoiceNo, Date purchaseDate, Long customerId, Long userId, String caseIds) {
		super();
		this.cartId = cartId;
		this.cartNo = cartNo;
		this.invoiceNo = invoiceNo;
		this.purchaseDate = purchaseDate;
		this.customerId = customerId;
		this.userId = userId;
		this.caseIds = caseIds;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "cart_id")
	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	@Column(name = "cart_no")
	public String getCartNo() {
		return cartNo;
	}

	public void setCartNo(String cartNo) {
		this.cartNo = cartNo;
	}

	@Column(name = "invoice_no")
	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	@Column(name = "purchase_date")
	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	@Column(name = "customer_id")
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	@Column(name = "user_id")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Column(name = "case_ids")
	public String getCaseIds() {
		return caseIds;
	}

	public void setCaseIds(String caseIds) {
		this.caseIds = caseIds;
	}

	@Transient
	public Set<Long> getSelectedCases() {
		return selectedCases;
	}

	public void setSelectedCases(Set<Long> selectedCases) {
		this.selectedCases = selectedCases;
	}
	
	
	
	
	
	
	

}
