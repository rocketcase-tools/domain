package com.rocketcase.clioclient.domain.chargebee;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;

@Entity
@Table(name = "customers")
public class Customer implements BaseEntity, java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3145088357194564403L;
	/**
	 * 
	 */

	@Transient
	public static final String URL_PRE_FIX = "customer";
	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";
	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";
	@Transient
	public static final String GET_BY_USER_URL = URL_PRE_FIX + "/user/";

	@Transient
	public static final String SAVE_URL = URL_PRE_FIX + "/save";

	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";
	public static final String GET_FOR_SYNC_URL = URL_PRE_FIX + "/get/syncable";

	/**
	 * 
	 */
	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String company;
	private String subscription;
	private String encInfo;
	private String config;

	private String autoLicenseClients;
	
	private String status;

	private String host;
	private String apiVersion;
	private String appKey;
	private String appSecret;
	private String authorizationCode;
	private String accessToken;
	private String tokenType;
	private Long accessTokenExpiresIn;
	private String refreshToken;
	private Date updatedAt;

	private String syncStatus;

	private Map<String, String> infoMap;
	private Map<String, String> configMap;

	public Customer() {
	}

	public Customer(Long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "company")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Column(name = "subscription")
	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	@Column(name = "enc_info")
	public String getEncInfo() {
		return encInfo;
	}

	public void setEncInfo(String encInfo) {
		this.encInfo = encInfo;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Transient
	public Map<String, String> getInfoMap() {
		return infoMap;
	}

	public void setInfoMap(Map<String, String> infoMap) {
		this.infoMap = infoMap;
	}

	@Column(name = "app_key")
	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	@Column(name = "app_secret")
	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	@Column(name = "authorization_code")
	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	@Column(name = "access_token")
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@Column(name = "access_token_expires_in")
	public Long getAccessTokenExpiresIn() {
		return accessTokenExpiresIn;
	}

	public void setAccessTokenExpiresIn(Long accessTokenExpiresIn) {
		this.accessTokenExpiresIn = accessTokenExpiresIn;
	}

	@Column(name = "token_type")
	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	@Column(name = "refresh_token")
	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	@Column(name = "api_version")
	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Transient
	private Map<String, Object> params;

	@Transient
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	@Transient
	public Map<String, Object> getParams() {
		return this.params;
	}

	@Column(name = "host")
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	@Column(name = "sync_status")
	public String getSyncStatus() {
		return syncStatus;
	}

	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus;
	}

	@Column(name = "config")
	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	@Transient
	public Map<String, String> getConfigMap() {
		return configMap;
	}

	public void setConfigMap(Map<String, String> configMap) {
		this.configMap = configMap;
	}

	@Column(name = "auto_licensed_client_ids")
	public String getAutoLicenseClients() {
		return autoLicenseClients;
	}

	public void setAutoLicenseClients(String autoLicenseClients) {
		this.autoLicenseClients = autoLicenseClients;
	}
	
	
	
	
	

}
