package com.rocketcase.clioclient.domain;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

 
@Entity
@Table(name = "configurations")
public class Configuration implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3399053241560765796L;
	private Long id;
	private String key;
	private String value;

	public Configuration() {
	}

	public Configuration(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "`key`")
	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Column(name = "`value`")
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Key: " + this.key + ";  Value: " + this.value;
	}

}
