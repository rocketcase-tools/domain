package com.rocketcase.clioclient.domain;
// default package

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TablesSyncedStatus
 */
@Entity
@Table(name = "synced_entities")
public class SyncedEntity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8270103981112384646L;
	private Long id;
	private String entity;
	private String syncedTimestamp;
	private String description;
	private Long customerId;

	public SyncedEntity() {
	}

	public SyncedEntity(String entity, String syncedTimestamp, String description) {

		this.entity = entity;
		this.syncedTimestamp = syncedTimestamp;
		this.description = description;
	}

	public SyncedEntity(String entity, String syncedTimestamp) {
		this.entity = entity;
		this.syncedTimestamp = syncedTimestamp;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "entity")
	public String getEntity() {
		return this.entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	@Column(name = "synced_timestamp")
	public String getSyncedTimestamp() {
		return this.syncedTimestamp;
	}

	public void setSyncedTimestamp(String syncedTimestamp) {
		this.syncedTimestamp = syncedTimestamp;
	}

	public String getDescription() {
		return description;
	}

	@Column(name = "description")
	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "customer_id")
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

}
