package com.rocketcase.clioclient.domain;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;

@Entity
@Table(name = "licensed_matters")
public class LicensedMatter implements BaseEntity, java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4006994170715764377L;

	@Transient
	public static final String URL_PRE_FIX = "licensedMatter";

	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";

	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";

	@Transient
	public static final String GET_BY_IDS_URL = URL_PRE_FIX + "/ids/";

	@Transient
	public static final String GET_BY_APPLICATION_URL = URL_PRE_FIX + "/application/";
	@Transient

	public static final String SAVE_URL = URL_PRE_FIX + "/save";

	@Transient
	public static final String SAVE_LIST_URL = URL_PRE_FIX + "/saveList";

	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";

	private Long id;
	private Long matterId;
	private Long customerId;
	private Date licenseDate;
	private String invoiceNo;
	private String cbInvoiceId;

	public LicensedMatter() {
	}

	public LicensedMatter(Long matterId, Long customerId, Date licenseDate, String invoiceNo, String cbInvoiceId) {
		super();
		this.matterId = matterId;
		this.customerId = customerId;
		this.licenseDate = licenseDate;
		this.invoiceNo = invoiceNo;
		this.cbInvoiceId = cbInvoiceId;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "matter_id")
	public Long getMatterId() {
		return matterId;
	}

	public void setMatterId(Long matterId) {
		this.matterId = matterId;
	}

	@Column(name = "license_date")
	public Date getLicenseDate() {
		return licenseDate;
	}

	public void setLicenseDate(Date licenseDate) {
		this.licenseDate = licenseDate;
	}

	@Column(name = "customer_id")
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	@Column(name = "invoice_no")
	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	@Column(name = "cb_invoice_id")
	public String getCbInvoiceId() {
		return cbInvoiceId;
	}

	public void setCbInvoiceId(String cbInvoiceId) {
		this.cbInvoiceId = cbInvoiceId;
	}
	
	

}
